'''
This source code was used to collect and analyze data for
"Using Social Media to Characterize Public Sentiment toward Cancer Screening Interventions" by Metwally O, Blumberg S, Ladabaum U, and Sinha SR. (JMIR)

Code that collects tweets is original.

Code that utilizes NLTK's Naive Bayes classifiers is based on the following:

http://www.laurentluce.com/posts/twitter-sentiment-analysis-using-python-and-nltk/

*Copyright and Licensing Information*

Attribution-NonCommercial 3.0 United States (CC BY-NC 3.0 US)
This is a human-readable summary of (and not a substitute for) the license. Disclaimer.


You are free to:

Share — copy and redistribute the material in any medium or format

Adapt — remix, transform, and build upon the material
The licensor cannot revoke these freedoms as long as you follow the license terms under the following terms:

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

NonCommercial — You may not use the material for commercial purposes.

No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
Notices:

You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.

No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.


Omar Metwally, M.D.
omar.metwally@gmail.com

'''


#coding:utf-8
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from twython import Twython
import time, datetime, numpy
import re, math, collections, itertools, os
import nltk, nltk.classify.util, nltk.metrics
from nltk.classify import NaiveBayesClassifier
from nltk.metrics import BigramAssocMeasures
from nltk.probability import FreqDist, ConditionalFreqDist
from klout import *
import urllib, random
import csv, operator
import networkx as nx

APP_KEY = ''
APP_SECRET = ''
OAUTH_TOKEN = ''
OAUTH_TOKEN_SECRET = ''

word_features = []
document_words = None
all_tweets = []

def parse_csv(filename):
    fieldnames = ['id','tweet_id','screen_name','name','klout_score','message','created_at','follower_count','retweet_count','favorite_count','location','Sid category','omar category','category','query']
    d = {}
    for f in fieldnames:
        d[f] = []
    dictReader = csv.DictReader(open(filename,'rb'),fieldnames=fieldnames,delimiter=',',quotechar='"')
    for row in dictReader:
       for key in row:
           d[key].append(row[key])

    return d

# START NEW 
def graph_retweets_vs_klout(dict_in,title,originals=False):
    
    pos_retweets = []
    neg_retweets = []
    neutral_retweets = []
    klout_list = []
    pos_klouts = []
    neg_klouts = []
    neutral_klouts = []

    #pattern = re.compile("RT @"+"[\S]+"+":")
    i = 1
    while i < len(dict_in['id']):
	message = dict_in['message'][i]
	user = dict_in['screen_name'][i]
	cat = dict_in['category'][i]
	retweets = int(dict_in['retweet_count'][i])
	try:
            k = float(dict_in['klout_score'][i])
	    cat = int(cat)
	except:
            k = 0
	    cat = 0
        if originals:
            if "RT @" in message: k = retweets = 0

        if retweets >0 and k > 0:
            if cat == 1: 
                pos_retweets.append(retweets)
                pos_klouts.append(k)
            elif cat == 2: 
                neg_retweets.append(retweets)
                neg_klouts.append(k)
            elif cat ==3: 
                neutral_retweets.append(retweets)
                neutral_klouts.append(k)

	i+=1

    #x = numpy.array(klout_list)
    #y = numpy.array(retweet_list)


    ax = plt.axes()
    ax.set_yscale('log')
    #ax.set_xscale('log')
    ax.set_ylabel('Retweets')
    ax.set_xlabel('Klout Score')
    #fit = numpy.polyfit(x,y,deg=1)
    #ax.plot(x, fit[0]*x+fit[1],color='red')
    #plt.scatter(klout_list,retweet_list,color=color,alpha=alpha)
    ax.scatter(neutral_klouts,neutral_retweets,color='blue',alpha=0.2)
    ax.scatter(pos_klouts,pos_retweets,color='green',alpha=0.7)
    ax.scatter(neg_klouts,neg_retweets,color='red',alpha=0.7)
    plt.title(title)
    plt.savefig(title+".png")


# END NEW

def graph(dict_in,title):
    
    G = nx.DiGraph()
    pos_nodes = []
    neg_nodes = []
    other_nodes = []
    pos_edges = []
    neg_edges = []
    other_edges = []
    labels = {}
    num_labels = 0
    MAX_LABELS = 0
    tweeters = []
    retweeters = []

    pattern = re.compile("@"+"[\S]+"+":")
    i = 1
    while i < len(dict_in['id']):
	message = dict_in['message'][i]
	user = dict_in['screen_name'][i]
	cat = dict_in['category'][i]
	retweets = int(dict_in['retweet_count'][i])
	try:
	    cat = int(cat)
	except:
	    cat = 0

	rt_user = None
	result = pattern.search(message)
	if result:
	    rt_user = result.group()[1:-1]
            if (rt_user not in tweeters) and (user not in rt_user):
                if cat == 1: 
                    pos_edges.append((user,rt_user))
                    tweeters.append(user)
                    retweeters.append(rt_user)
                elif cat == 2: 
                    neg_edges.append((user,rt_user))
                    tweeters.append(user)
                    retweeters.append(rt_user)
                else: 
                    other_edges.append((user,rt_user))
                    tweeters.append(user)
                    tweeters.append(rt_user)

                if retweets > 1500 and len(labels) < MAX_LABELS:
                    #labels[user] = user
                    labels[rt_user]=rt_user
                    num_labels +=1
                G.add_edge(*(rt_user,user))

        '''
	else:
	    if cat ==1: pos_nodes.append(user)
	    elif cat==2: neg_nodes.append(user)
	    else: other_nodes.append(user)
	    if retweets > 1500 and len(labels) < MAX_LABELS:
	    	labels[user] = user
		num_labels+=1
	    G.add_node(user)
        '''

	i+=1

    #draw_and_save_graph(G,title,labels,pos_nodes,neg_nodes,other_nodes,pos_edges,neg_edges,other_edges)
    return (G,pos_nodes,neg_nodes,pos_edges,neg_edges)

#graph_dict_in is a dictionary returned by parse_csv
def graph_analytics(graph_dict_in):
    (g,pn,nn,pe,ne) = graph(graph_dict_in,"Mammography Twitter Network Analysis")
    sorted_x = sorted(nx.out_degree_centrality(g).items(), key=operator.itemgetter(1))
    pos_user = []
    neg_user = []
    neutral_user = []
    pos_centrality = []
    neg_centrality = [] 
    neutral_centrality = []

    for key,value in sorted_x:
        if key in pn or key in pe:
            if value > 0.0:
                pos_user.append(key)
                pos_centrality.append(value)
        elif key in nn or key in ne:
            if value > 0.0:
                neg_user.append(key)
                neg_centrality.append(value)
        else:
            neutral_user.append(key)
            neutral_centrality.append(value)

    print "--------Out degree centrality--------"
    print "sum of positive ODC: "+str(numpy.mean(pos_centrality))+", std dev: "+str(numpy.std(pos_centrality))
    print "sum of negative ODC: "+str(numpy.mean(neg_centrality))+", std dev: "+str(numpy.std(neg_centrality))

    return (pos_centrality,neg_centrality)
    average_clustering = nx.average_clustering(g.to_undirected())

    # BETWEENNNESS
    del pos_user[:]
    del neg_user[:]
    del neutral_user[:]

    pos_betweenness = []
    neg_betweenness = []
    neutral_betweenness = []

    betweenness_centrality = sorted(nx.betweenness_centrality(g.to_undirected().items(), key=operator.itemgetter(1)))
    for key,value in betweenness_centrality:
        pass

    # CLOSNESS CENTRALITY

    closeness = sorted(nx.closeness_centrality(g.to_undirected().items(), key=operator.itemgetter(1)))


    cliques = nx.find_cliques(g.to_undirected())
    for c in cliques:
        for element in c:
            # c is a Twitter screen name
            pass

def draw_and_save_graph(graph,title,labels,pos_nodes,neg_nodes,other_nodes,pos_edges,neg_edges,other_edges):
    pos=nx.spring_layout(graph)

    nx.draw_networkx_edges(graph,pos,edgelist=other_edges,edge_color='b',width=5, alpha=0.4)
    nx.draw_networkx_edges(graph,pos,edgelist=pos_edges,edge_color='g',width=5, alpha=0.4)
    nx.draw_networkx_edges(graph,pos,edgelist=neg_edges,edge_color='r',width=5, alpha=0.4)

    nx.draw_networkx_nodes(graph,pos,nodelist=other_nodes,node_color='b',alpha=0.4,node_size=25)
    nx.draw_networkx_nodes(graph,pos,nodelist=pos_nodes,node_color='g',alpha=0.4,node_size=25)
    nx.draw_networkx_nodes(graph,pos,nodelist=neg_nodes,node_color='r',alpha=0.4,node_size=25)

    nx.draw_networkx_labels(graph,pos,labels,font_size=16,label_pos=random.uniform(0.0,1.0))
    plt.axis('off')
    plt.title(title)  
    plt.savefig(title+".png",format="PNG") 
    #plt.show()



def populate_klout_scores():
    twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    tweets = db(db.mammography_tweets).select(limitby=(0,10)) 
    k = Klout('kn9rdzrncqe4rxsqsnw5pnzy')
    scores = []

    for tweet in tweets:
        screen_name = tweet['screen_name']
        
        kloutId = k.identity.klout(screenName=screen_name).get('id')
        score = k.user.score(kloutId=kloutId).get('score')
        scores.append(score)
        time.sleep(0.5)
    response.flash = "mean klout score for mammography_tweets: " + str(sum(scores))
    
    return dict()
    
def populate_colonoscopy():
    
    twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    query = 'colonoscopy'
    query_list = ['colonoscopy', 'screening colonoscopy', 'first colonoscopy', 'surveillance colonoscopy', 'crohnâ€™s disease AND colonoscopy', 'colonoscopy AND CD', 'colonoscopy', 'crohnâ€™s AND colonoscopy', 'ulcerative colitis AND colonoscopy', 'colonoscopy AND UC', 'IBD AND colonoscopy', 'colonoscopy AND magnesium citrate', 'colonoscopy AND golytely', 'colonoscopy AND halflytely', 'colonoscopy AND miralax', 'colonoscopy AND moviprep', 'colonoscopy AND low volume prep']

    if not session.query_index_colonoscopy: session.query_index_colonoscopy = 0
    #session.next_counter moves on to the next query term in the list after X refreshes        
    if not session.next_counter_colonoscopy: session.next_counter_colonoscopy = 0 
    #session.query_index_colonoscopy = random.randint(0,len(query_list)-1)
    if session.query_index_colonoscopy > len(query_list)-1: session.query_index_colonoscopy = 0
    query = urllib.quote_plus(query_list[session.query_index_colonoscopy])   

    if 'result' in locals() and not session.lowest_min_id_colonoscopy:
        result = twitter.search(q=query,count=100,lang='en')
        session.lowest_min_id_colonoscopy = result['statuses'][0]['id']
    else:
        result = twitter.search(q=query,count=100,lang='en',max_id=session.lowest_min_id_colonoscopy)
    
    for x in result['statuses']:
        if x['id'] < session.lowest_min_id_colonoscopy: session.lowest_min_id_colonoscopy = x['id']            

        dt = datetime.datetime.strptime(x['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
        #db.colonoscopy_tweets.validate_and_insert(tweet_id=x['id'], message=x['text'], created_at=dt, followers_count=x['user']['followers_count'], retweet_count=x['retweet_count'], screen_name = x['user']['screen_name'] )
        
        db.colonoscopy_v2.validate_and_insert(tweet_id=x['id'], message=x['text'], created_at=dt, followers_count=x['user']['followers_count'], retweet_count=x['retweet_count'], favorite_count=x['favorite_count'], screen_name = x['user']['screen_name'], name=x['user']['name'], location=x['user']['location'], query=query )

    session.next_counter_colonoscopy +=1
    if session.next_counter_colonoscopy > 10:
        session.next_counter_colonoscopy = 0
        session.query_index_colonoscopy +=1
        session.lowest_min_id_colonoscopy = None
    
    response.flash = "query term: "+query+" with "+str(twitter.get_lastfunction_header('x-rate-limit-remaining'))+" remaining queries in this 15 minute window"+", next_counter_colonoscopy: "+str(session.next_counter_colonoscopy)+", query_index: "+str(session.query_index_colonoscopy) #session.lowest_min_id                      
            
    return dict()

def populate_mammography():
    
    twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    query = 'mammogram'
    query_list = ['mammogram', 'mammography']

    if not session.query_index_mammography: session.query_index_mammography = 0
    if not session.next_counter_mammography: session.next_counter_mammography = 0
    #session.query_index = random.randint(0,len(query_list)-1)        
    if session.query_index_mammography > len(query_list)-1: session.query_index_mammography = 0
    query = urllib.quote_plus(query_list[session.query_index_mammography])   

    if 'result' in locals() and not session.lowest_min_id_mammography:
        result = twitter.search(q=query,count=100,lang='en')
        session.lowest_min_id_mammography = result['statuses'][0]['id']
    else:
        result = twitter.search(q=query,count=100,max_id=session.lowest_min_id_mammography,lang='en')
    
    for x in result['statuses']:
        if x['id'] < session.lowest_min_id_mammography: session.lowest_min_id_mammography = x['id']
        #kloutId = k.identity.klout(screenName=x['user']['screen_name']).get('id')
        #kloutScore = k.user.score(kloutId=kloutId).get('score')                   

        dt = datetime.datetime.strptime(x['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
        db.mammography_v2.validate_and_insert(tweet_id=x['id'], message=x['text'], created_at=dt, followers_count=x['user']['followers_count'], retweet_count=x['retweet_count'], favorite_count=x['favorite_count'], screen_name = x['user']['screen_name'], name=x['user']['name'], location=x['user']['location'], query=query )

    session.next_counter_mammography +=1
    if session.next_counter_mammography > 10:
        session.next_counter_mammography = 0
        session.query_index_mammography +=1
        session.lowest_min_id_mammography = None
        
    response.flash = "query term: "+query+" with "+str(twitter.get_lastfunction_header('x-rate-limit-remaining'))+" remaining queries in this 15 minute window"+", next_counter: "+str(session.next_counter_mammography)+", query_index_mammography: "+str(session.query_index_mammography) #session.lowest_min_id                      
            
    return dict()

# populate PAP tweets
def populate_pap():
    
    twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    query = 'pap smear'
    query_list = ['pap smear', 'pap test', 'pap screen', 'pap screening', 'papanicolau screen', 'papanicolau screening', 'papanicolau test', 'pap AND cervical cancer', 'pap AND pelvic exam', 'pap AND hpv', 'pap AND human papilloma virus']

    if not session.query_index_pap: session.query_index_pap = 0
    if not session.next_counter_pap: session.next_counter_pap = 0
        
    #session.query_index = random.randint(0,len(query_list)-1)        
    if session.query_index_pap > len(query_list)-1: session.query_index_pap = 0
    query = urllib.quote_plus(query_list[session.query_index_pap])       

    if 'result' in locals() and not session.lowest_min_id_pap:
        result = twitter.search(q=query,count=100,lang='en')
        session.lowest_min_id_pap = result['statuses'][0]['id']
    else:
        result = twitter.search(q=query,count=100,max_id=session.lowest_min_id_pap,lang='en')
    
    for x in result['statuses']:
        if x['id'] < session.lowest_min_id_pap: session.lowest_min_id_pap = x['id']
        #kloutId = k.identity.klout(screenName=x['user']['screen_name']).get('id')
        #kloutScore = k.user.score(kloutId=kloutId).get('score')                   

        dt = datetime.datetime.strptime(x['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
        db.pap_v2.validate_and_insert(tweet_id=x['id'], message=x['text'], created_at=dt, followers_count=x['user']['followers_count'], retweet_count=x['retweet_count'], favorite_count=x['favorite_count'], screen_name = x['user']['screen_name'], name=x['user']['name'], location=x['user']['location'], query=query )

    session.next_counter_pap +=1
    if session.next_counter_pap > 10:
        session.next_counter_pap = 0
        session.query_index_pap +=1
        session.lowest_min_id_pap = None
        
    response.flash = "query term: "+query+" with "+str(twitter.get_lastfunction_header('x-rate-limit-remaining'))+" remaining queries in this 15 minute window"+", next_counter_pap: "+str(session.next_counter_pap)+", query_index_pap: "+str(session.query_index_pap) #session.lowest_min_id                      
            
    return dict()

def get_words_in_tweets(tweets):
    all_words = []
    for (words, sentiment) in tweets:
        all_words.extend(words)
    return all_words

def get_word_features(wordlist):
    wordlist = nltk.FreqDist(wordlist)
    word_features = wordlist.keys()
    return word_features

def extract_features(document):
    global document_words

#    if 'document_words' not in globals():
    pos_tweets = []
    other_tweets = []
#    tweets = []
    
    if len(all_tweets) == 0:
        results = parse_csv('db_colonoscopy_v2.csv')
        i = 0
        while i < len(results['id']):
            try:
                cat = int(results['omar_category'][i])
                msg = results['message'][i]
            except:
                cat = None

            if cat == 1:
                pos_tweets.append((msg,'positive'))
            elif cat == 2 or cat ==3:
                other_tweets.append((msg,'other'))
            i+=1

        for (words, sentiment) in pos_tweets + other_tweets:
            words_filtered = clean_string(words) #[e.lower() for e in words.split() if len(e) >= 3]
            all_tweets.append((words_filtered, sentiment))
    
    document_words = set(document)
    
    features = {}
 
    word_features = get_word_features(get_words_in_tweets(all_tweets))
    print word_features
    for word in word_features:
        features['contains(%s)' % word] = (word in document_words)
    return features

def experiment():
    '''
    db.positive_followers.drop()
    db.negative_followers.drop()
    db.positive_retweets.drop()
    db.negative_retweets.drop()
    
    db.positive_prep_followers.drop()
    db.negative_prep_followers.drop()
    db.positive_prep_retweets.drop()
    db.negative_prep_retweets.drop()    
    '''
    
    pos_tweets = []
    neg_tweets = []
    pos_results = db((db.tweets.category==1)).select(limitby=(0,2127))
    for result in pos_results: pos_tweets.append((result.message,'positive'))
    neg_results = db((db.tweets.category==2)).select(limitby=(0,2127))
    for result in neg_results: neg_tweets.append((result.message,'negative'))
    tweets = []
     
    for (words, sentiment) in pos_tweets + neg_tweets:
        words_filtered = clean_string(words) 
        tweets.append((words_filtered, sentiment))
    
    word_features = get_word_features(get_words_in_tweets(tweets))
    training_set = nltk.classify.apply_features(extract_features,tweets)
    classifier = nltk.NaiveBayesClassifier.train(training_set) 
    
    test_tweets = db(db.tweets).select(limitby=(0,2127))
    num_positive = num_negative = 0
    positive_num_prep = 0
    negative_num_prep = 0
    positive_followers = []
    positive_prep_followers = []
    positive_retweets = [] 
    positive_prep_retweets = []
    negative_followers = []
    negative_prep_followers = []
    negative_retweets = []
    negative_prep_retweets = []

    for tweet in test_tweets: 
        category = classifier.classify(extract_features(clean_string(tweet.message)) )
        if category == 'positive': 
            
            if 'prep' in tweet.message.lower(): 
                positive_num_prep += 1
                positive_prep_followers.append(tweet.followers_count)
                positive_prep_retweets.append(tweet.retweet_count)
                #db.positive_prep_followers.insert(followers=tweet.followers_count)
                #db.positive_prep_retweets.insert(retweets=tweet.retweet_count)
                
            else:
                num_positive += 1
                positive_followers.append(tweet.followers_count)
                positive_retweets.append(tweet.retweet_count)
                #db.positive_followers.insert(followers=tweet.followers_count)
                #db.positive_retweets.insert(retweets=tweet.retweet_count)                

        elif category == 'negative':

            
            if 'prep' in tweet.message.lower(): 
                negative_num_prep += 1
                negative_prep_followers.append(tweet.followers_count)
                negative_prep_retweets.append(tweet.retweet_count)
                #db.negative_prep_followers.insert(followers=tweet.followers_count)
                #db.negative_prep_retweets.insert(retweets=tweet.retweet_count)
            else:
                num_negative +=1
                negative_followers.append(tweet.followers_count) 
                negative_retweets.append(tweet.retweet_count)
                #db.negative_followers.insert(followers=tweet.followers_count)
                #db.negative_retweets.insert(retweets=tweet.retweet_count)                
    
    str1 = "number of positive tweets: " + str(num_positive) + "\nnumber of negative tweets: " + str(num_negative) + "\n" + "mean positive retweets: " + str(sum(positive_retweets) / float(len(positive_retweets))) + "\n" + "mean negative retweets: " + str(sum(negative_retweets) / float(len(negative_retweets))) + "\n" 

    str2 = "std dev colonoscopy median positive followers: " + str(numpy.median(positive_followers)) + "\n" + "std dev colonoscopy median negative followers: " + str(numpy.median(negative_followers)) #+ "\n" + "std dev colonoscopy positive retweets: " + str(numpy.std(positive_retweets)) + "\n" + "std dev colonoscopy negative retweets: " + str(numpy.std(negative_retweets))
    
    str3 = "std dev colonoscopy positive retweets: " + str(numpy.std(positive_retweets)) + "\n" + "std dev colonoscopy negative retweets: " + str(numpy.std(negative_retweets))
    
    # PREP-RELATED TWEETS
    
    str1_prep = "number of positive prep tweets: " + str(positive_num_prep) + "\nnumber of negative prep tweets: " + str(negative_num_prep) + "\n" + "mean positive prep retweets: " + str(sum(positive_prep_retweets) / float(len(positive_prep_retweets))) + "\n" + "mean negative prep retweets: " + str(sum(negative_prep_retweets) / float(len(negative_prep_retweets))) + "\n" 

    str2_prep = "std dev colonoscopy prep median positive followers: " + str(numpy.median(positive_prep_followers)) + "\n" + "std dev colonoscopy prep median negative followers: " + str(numpy.median(negative_prep_followers)) #+ "\n" + "std dev colonoscopy positive retweets: " + str(numpy.std(positive_retweets)) + "\n" + "std dev colonoscopy negative retweets: " + str(numpy.std(negative_retweets))
    
    str3_prep = "std dev colonoscopy prep positive retweets: " + str(numpy.std(positive_prep_retweets)) + "\n" + "std dev colonoscopy prep negative retweets: " + str(numpy.std(negative_prep_retweets))

           
    return dict(result = str1 + "\n\n" + str2 + "\n\n" + str3 + "\n\n\n" + str1_prep + "\n\n"+str2_prep+"\n\n"+str3_prep)

def statistics():
#    response.flash = str(numpy.std([51.37,51.86,44.31,64.91,59.93,58.19,71.77,52.4,48.66,60.36])) 
    #response.flash = str(numpy.std([86.49,55.55,55.41,67.37,78.94,58.1,78.85,60.10,60.42,65.96]))
    response.flash = str(numpy.std([86.49,78.85,60.42,67.37,78.94]))
    #response.flash = str(numpy.std([51.37,48.66,44.31,64.91,59.93]))

    return dict()

def experiment_positive_or_other():
    pos_tweets = []
    other_tweets = []
    tweets = []
    results = parse_csv('db_colonoscopy_v2.csv')

    i = 0
    while i < len(results['id']):
        try:
            cat = int(results['omar_category'][i])
            msg = results['message'][i]
        except: 
            cat = None
            pass
        if cat == 1:
            pos_tweets.append((msg,'positive'))
        elif cat ==2 or cat == 3: 
            other_tweets.append((msg,'other'))
        i+=1

    print "Number of positively labeled tweets: "+str(len(pos_tweets))
    print "Number of otherly labeled tweets: "+str(len(other_tweets))

    for (words, sentiment) in pos_tweets + other_tweets:
        words_filtered = clean_string(words) 
        tweets.append((words_filtered, sentiment))
    
    word_features = get_word_features(get_words_in_tweets(tweets))
    training_set = nltk.classify.apply_features(extract_features,tweets)
    classifier = nltk.NaiveBayesClassifier.train(training_set) 
    test_tweets = parse_csv('db_colonoscopy_v2.csv')
    num_positive = num_other = 0
    positive_num_prep = 0
    negative_num_prep = 0
    positive_followers = []
    positive_retweets = [] 
    negative_followers = []
    negative_retweets = []

    i = 0
    while i < len(test_tweets['id']):
        category = classifier.classify(extract_features(clean_string(test_tweets['message'][i])))
        if category == 'positive':
            num_positive +=1
            positive_followers.append(test_tweets['follower_count'][i])
            positive_retweets.append(test_tweets['retweet_count'][i])
        elif category == 'other':
            num_other +=1
            negative_followers.append(test_tweets['follower_count'][i])
            negative_retweets.append(test_tweets['retweet_count'][i])

        i+=1
    
    print "Number of positive tweets: " + str(num_positive)
    print "Number of negative tweets: "+ str(num_other)

def analyze():
    pos_tweets = []
    neg_tweets = []
    
    num_positive = db(db.tweets.category==1).count()
    num_negative = db(db.tweets.category==2).count()

    true_positive = true_negative = false_positive = false_negative = 0
    
    pos_results = db(db.tweets.category==1).select(orderby='<random>')
    for result in pos_results[:int(num_positive/1.3)]: pos_tweets.append((result.message,'positive'))
    neg_results = db(db.tweets.category==2).select(orderby='<random>')
    for result in neg_results[:int(num_negative/1.3)]: neg_tweets.append((result.message,'negative'))
    tweets = []

    for (words, sentiment) in pos_tweets + neg_tweets:
        #words_filtered = [re.findall(r"[\w']+|[.,!?;]", e.lower().rstrip()) for e in words.split() if len(e) >=3]

        words_filtered = clean_string(words) #[e.lower() for e in words.split() if len(e) >= 3]
        tweets.append((words_filtered, sentiment))
    
    word_features = get_word_features(get_words_in_tweets(tweets))
    training_set = nltk.classify.apply_features(extract_features,tweets)
    classifier = nltk.NaiveBayesClassifier.train(training_set) 
    #test_tweets = pos_results[num_positive/2 : ] + neg_results[num_negative/2:] #db(db.tweets.category!=0).select()
    blah = []
    for tweet in  pos_results[int(num_positive/1.3) : ]:  #test_tweets:
        category = classifier.classify(extract_features(clean_string(tweet.message)) )
        blah.append(tweet.category)
        
        if category =='negative': 
            if tweet.category == 2: true_negative += 1
            else: false_negative +=1
        elif category =='positive': 
            if tweet.category == 1: true_positive +=1
            else: false_positive += 1

    for tweet in  neg_results[int(num_negative/1.3) : ]:  #test_tweets:
        category = classifier.classify(extract_features(clean_string(tweet.message)) )
        if category =='negative': 
            if tweet.category == 2: true_negative += 1
            else: false_negative +=1
        elif category =='positive': 
            if tweet.category == 1: true_positive +=1
            else: false_positive += 1
                
    #response.flash = classifier.classify(extract_features(test_tweets[0].message.split()) )
    sensitivity = float(true_positive) / (true_positive + false_negative)
    specificity = float(true_negative) / (true_negative + false_positive)
    #response.flash = "sensitivity: " + str(sensitivity) + "\nspecificity: " + str(specificity)
    #response.flash = "accuracy: " + str(float(true_positive + true_negative) / (true_positive + true_negative + false_positive + false_negative))
    response.flash = str(len(pos_tweets+neg_tweets))
    
    #response.flash = "true positive: " + str(true_positive) + "\ntrue negative: " + str(true_negative) + "\nfalse positive: " + str(false_positive) + "\nfalse negative: " + str(false_negative)
    #response.flash = blah
    
    return dict()


def clean_string(string_in):
    return re.findall(r"[\w']+|[.,!?;]", string_in.rstrip())


def plot_freq_vs_chainsize(dict_in,title):
    pos_retweets = []
    neg_retweets = []
    other_retweets = []

    i = 1

    while i < len(dict_in['id']):
        retweets = dict_in['retweet_count'][i]
        cat = dict_in['category'][i]
        msg = dict_in['message'][i]

        try:
            retweets = int(retweets)
            cat = int(cat)
        except:
            retweets = None
            cat = int(cat)
        if retweets == 0: retweets = None

        if retweets and cat and "RT @" not in msg:
            if cat ==1:
                pos_retweets.append(retweets)
            if cat ==2:
                neg_retweets.append(retweets)
            elif cat ==3:
                other_retweets.append(retweets)

        i+=1

    n,bins,patches = plt.hist([pos_retweets,neg_retweets,other_retweets],50,normed=False,alpha=1,color=['green','crimson','blue'])
    plt.xlabel('Retweets')
    plt.ylabel('Frequency')
    plt.title(title) 
    plt.grid(True)
    plt.savefig(title+".png") 




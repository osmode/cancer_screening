'''
This source code was used to collect and analyze data for
"Using Social Media to Characterize Public Sentiment toward Cancer Screening Interventions" by Metwally O, Blumberg S, Ladabaum U, and Sinha SR. (JMIR)

Code that collects tweets is original.

Code that utilizes NLTK's Naive Bayes classifiers is based on the following:

http://www.laurentluce.com/posts/twitter-sentiment-analysis-using-python-and-nltk/

*Copyright and Licensing Information*

Attribution-NonCommercial 3.0 United States (CC BY-NC 3.0 US)
This is a human-readable summary of (and not a substitute for) the license. Disclaimer.


You are free to:

Share — copy and redistribute the material in any medium or format

Adapt — remix, transform, and build upon the material
The licensor cannot revoke these freedoms as long as you follow the license terms under the following terms:

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

NonCommercial — You may not use the material for commercial purposes.

No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
Notices:

You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.

No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.


Omar Metwally, M.D.
omar.metwally@gmail.com

'''


#coding:utf-8
from twython import Twython
import time, datetime, numpy
import re, math, collections, itertools, os
import nltk, nltk.classify.util, nltk.metrics
from nltk.classify import NaiveBayesClassifier
from nltk.metrics import BigramAssocMeasures
from nltk.probability import FreqDist, ConditionalFreqDist
from klout import *
import urllib, random
import csv, math
from unidecode import unidecode
import matplotlib.pyplot as plt
from nltk.metrics.scores import f_measure
from happyfuntokenizing import *

APP_KEY = 'yourkey'
APP_SECRET = 'yoursecret'
OAUTH_TOKEN = 'youroathtoken' 
OAUTH_TOKEN_SECRET = 'tokensecret'

word_features = []
document_words = None
all_tweets = []
fieldnames = ['id','tweet_id','screen_name','name','klout_score','message','created_at','follower_count','retweet_count','favorite_count','location','Sid category','omar category','category','query']
dp_fieldnames = ['screen_name']
prep_words = ['prep','bowel prep','golytely','halflytely','mag citrate','magnesium citrate','moviprep']

# These are the labeled files
'''
colonoscopy_csv_filename = "colo_labeled.csv"  #"db_colonoscopy_v2.csv"
pap_csv_filename = "pap_labeled.csv"  #"db_pap_v2.csv"
mammography_csv_filename = "mammo_labeled.csv"  #"db_mammography_v2.csv"
'''

colonoscopy_csv_filename = "colo_train.csv"
pap_csv_filename = "pap_train.csv"
mammography_csv_filename = "mammo_train.csv" 

# Current experiment
CURRENT_CSV_FILE =  pap_csv_filename #colonoscopy_csv_filename #mammography_csv_filename    #pap_csv_filename

# ALL_CSV_FILE contains tweets to be labeled
#ALL_CSV_FILE =  "colo.csv"  #"mammo.csv" #"pap.csv" 

ALL_CSV_FILE = "pap_test.csv"  #"colo_test.csv"

CURRENT_CSV_FILE_A= CURRENT_CSV_FILE.split('.')[0]+"_A"+".csv"
CURRENT_CSV_FILE_B = CURRENT_CSV_FILE.split('.')[0]+"_B"+".csv"
CURRENT_CSV_FILE_C = CURRENT_CSV_FILE.split('.')[0]+"_C"+".csv"
CURRENT_CSV_FILE_K = CURRENT_CSV_FILE.split('.')[0]+"_K"+".csv" 
CURRENT_CSV_FILE_FINAL = ALL_CSV_FILE.split('.')[0]+"_final"+".csv"

tok = Tokenizer(preserve_case=True)

def parse_csv(filename):

    d = {}
    if type(filename) is not type([]): filename = [filename]
    for fn in filename:
        for f in fieldnames:
            d[f] = []
        dictReader = csv.DictReader(open(fn,'rb'),fieldnames=fieldnames,delimiter=',',quotechar='"')
        for row in dictReader:
           for key in row:
               d[key].append(row[key])

    return d


# the contingency method 
def contingency():

    classifier1 = parse_csv(CURRENT_CSV_FILE_A)
    classifier2 = parse_csv(CURRENT_CSV_FILE_B)     
    gold_standard = parse_csv("pap_test.csv")    # labeled
    classifier1_dict = {}
    classifier2_dict = {}
    gold_dict = {}

    '''
    if len(classifier1['id']) != len(classifier2['id']):
        print "Unable to reconcile these two files! They are not the same length"
        return 0
    '''

    # load labeled set into a dictionary
    # then iterate through that dictionary and compare the labels with the classifier labels
    
    i = 1
    while i < len(gold_standard['id']):
        gold_dict[gold_standard['tweet_id'][i]] = gold_standard['omar category'][i] 
        i+=1
    i = 1
    while i < len(classifier1['id']):
        classifier1_dict[classifier1['tweet_id'][i]] = classifier1['category'][i]
        i+=1
    i = 1
    while i < len(classifier2['id']):
        classifier2_dict[classifier2['tweet_id'][i]] = classifier2['category'][i]
        print classifier2['category'][i]
        i+=1 
    
    a = 0
    b = 0
    c = 0
    d = 0
    e = 0
    f = 0
    g = 0
    h = 0
    i = 0

    for key,value in gold_dict.iteritems():
        curr_id = key
        actual_category = value
        classifier_category = '3'
        if curr_id in classifier1_dict and curr_id in classifier2_dict:
            if '1' in classifier1_dict[curr_id] : classifier_category = str(1)
            if '2' in classifier2_dict[curr_id]: classifier_category = str(2)
            
            if '1' in actual_category and '1' in classifier_category : a+=1
            elif '1' in actual_category and '3' in classifier_category : b+=1
            elif '1' in actual_category and '2' in classifier_category : c+=1
            elif '3' in actual_category and '1' in classifier_category : d+=1
            elif '3' in actual_category and '3' in classifier_category : e+=1
            elif '3' in actual_category and '2' in classifier_category : f+=1
            elif '2' in actual_category and '1' in classifier_category : g+=1
            elif '2' in actual_category and '3' in classifier_category : h+=1
            elif '2' in actual_category and '2' in classifier_category :
                i+=1
            
    print a, b, c, d, e, f, g, h, i
    return classifier1_dict, classifier2_dict, gold_dict
    #return classifier1_dict, classifier2_dict, gold_dict

def reconcile():
    num_pos = num_neg = num_neutral = num_conflicting = 0

    d1 = parse_csv(CURRENT_CSV_FILE_A)
    d2 = parse_csv(CURRENT_CSV_FILE_B)
    if len(d1['id']) != len(d2['id']):
        print "Unable to reconcile these two files! They are not the same length"
        return 0

    csvfile = open(CURRENT_CSV_FILE_C,"w")
    spamwriter = csv.writer(csvfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(fieldnames)

    i = 1
    while i < len(d1['id']):
        id1 = d1['tweet_id'][i]
        id2 = d2['tweet_id'][i]
        if id1 != id2:
            print "The lists to reconcile must be in the same order!"
            return 0

        cat1 = int(d1['category'][i])
        cat2 = int(d2['category'][i])
        final_cat = None
        line = []

        if cat1 ==1 or cat1==2 and cat2==3:
            final_cat = cat1
            if cat1 == 1: num_pos +=1
            if cat1 ==2: num_neg +=1

        elif cat1==3 and cat2==1 or cat2==2:
            final_cat = cat2
            if cat2 == 1: num_pos +=1
            if cat2 ==2: num_neg +=1

        elif cat1 ==3 and cat2 ==3:
            final_cat = 3
            num_neutral +=1
        elif cat1 == 1 and cat2 ==2 or cat1==2 and cat2==1:
            final_cat = 4
            print "Conflict between "+str(cat1)+" and "+str(cat2)
            num_conflicting+=1

        for k in fieldnames:
            if k is 'category':
                line.append(str(final_cat))
            else:
                line.append(d1[k][i])

        spamwriter.writerow(line)
        i+=1

    csvfile.close()
    print "Number positive: "+str(num_pos)
    print "Number negative: "+str(num_neg)
    print "Number neutral: "+str(num_neutral)
    print "Number conflicting: "+str(num_conflicting)

def reconcile_klout(klout_file_in,klout_file_out):

    d1 = parse_csv(klout_file_in)
    d2 = parse_csv(klout_file_out)
    klout_dict = {}
    if len(d1['id']) != len(d2['id']):
        print "Unable to reconcile these two files! They are not the same length"
        return 0
    if "_K" not in klout_file_in or "_K" in klout_file_out:
        print "Are you sure klout_file_in and klout_file_out are specified correctly?"
        return 0

    csvfile = open(CURRENT_CSV_FILE_FINAL,"w")
    spamwriter = csv.writer(csvfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(fieldnames)

    i = 1
    while i < len(d1['id']):
        username = d1['screen_name'][i]
        klout_score = d1['klout_score'][i]
        try: klout_score = float(klout_score)
        except: klout_score = ''
        if klout_score and username:
            klout_dict[username] = klout_score
        i += 1

    i = 1
    while i < len(d2['id']):
        line = []
        username = d2['screen_name'][i]

        for k in fieldnames:
            if k is 'klout_score' and username in klout_dict.keys():
                line.append(str(klout_dict[username]))
                print "Updating klout score = "+str(klout_dict[username])+" for "+username
            else:
                line.append(d2[k][i])

        spamwriter.writerow(line)
        i+=1

    csvfile.close()

def populate_klout_scores(filein=None):
    twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    k = Klout('yagz8hthp6v28b6zm775x')

    if filein: results = parse_csv(filein)
    else: results = parse_csv(CURRENT_CSV_FILE_C)

    csvfile = open(CURRENT_CSV_FILE_K,"w")
    spamwriter = csv.writer(csvfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(fieldnames)

    i = 1
    while i < len(results['id']):
        screen_name = results['screen_name'][i]
        klout_score = results['klout_score'][i]

        if "<NULL>" in klout_score or not klout_score:
            try:
                kloutId = k.identity.klout(screenName=screen_name).get('id')
                score = k.user.score(kloutId=kloutId).get('score')
            except:
                score = ''
            line = []

            for fn in fieldnames:
                if fn is 'klout_score':
                    line.append(str(score))
                    print i, score

                else:
                    line.append(results[fn][i])

            spamwriter.writerow(line)
            time.sleep(0.2)
        else:
            print klout_score+" for user "+screen_name+" was not updated"

        i+=1

    csvfile.close()

def get_words_in_tweets(tweets):
    all_words = []
    for (words, sentiment) in tweets:
        all_words.extend(words)
    return all_words

def get_word_features(wordlist):
    wordlist = nltk.FreqDist(wordlist)
    word_features = wordlist.keys()
    return word_features

def extract_features(document):
    global document_words

#    if 'document_words' not in globals():
    pos_tweets = []
    other_tweets = []
#    tweets = []

    if len(all_tweets) == 0:
        results = parse_csv(CURRENT_CSV_FILE)
        i = 1
        while i < len(results['id']):
            cat = results['omar category'][i]
            msg = unicode(results['message'][i],"utf-8")
            try:
                cat = int(cat)
            except:
                cat = None

            if cat == 1:
                pos_tweets.append((msg,'positive'))
            elif cat == 2 or cat ==3:
                other_tweets.append((msg,'other'))
            i+=1

        for (words, sentiment) in pos_tweets + other_tweets:
            words_filtered = clean_string(words) #[e.lower() for e in words.split() if len(e) >= 3]
            all_tweets.append((words_filtered, sentiment))

    document_words = set(document)

    features = {}

    word_features = get_word_features(get_words_in_tweets(all_tweets))
    for word in word_features:
        features['contains(%s)' % word] = (word in document_words)
    return features

def extract_features2(document):
    global document_words

#    if 'document_words' not in globals():
    neg_tweets = []
    other_tweets = []
#    tweets = []

    if len(all_tweets) == 0:
        results = parse_csv(CURRENT_CSV_FILE)
        i = 1
        while i < len(results['id']):
            cat = results['omar category'][i]
            msg = unicode(results['message'][i],"utf-8")
            try:
                cat = int(cat)
            except:
                cat = None

            if cat == 2:
                neg_tweets.append((msg,'negative'))
            elif cat == 1 or cat ==3:
                other_tweets.append((msg,'other'))
            i+=1

        for (words, sentiment) in neg_tweets + other_tweets:
            words_filtered = clean_string(words) #[e.lower() for e in words.split() if len(e) >= 3]
            all_tweets.append((words_filtered, sentiment))

    document_words = set(document)

    features = {}

    word_features = get_word_features(get_words_in_tweets(all_tweets))
    for word in word_features:
        features['contains(%s)' % word] = (word in document_words)
    return features

def experiment_positive_or_other():
    pos_tweets = []
    other_tweets = []
    new_cat_labels = []
    tweets = []
    results = parse_csv(CURRENT_CSV_FILE)  #parse_csv([pap_csv_filename,colonoscopy_csv_filename,mammography_csv_filename]))
    tp = 0
    tn = 0
    fp = 0
    fn = 0

    i = 1
    while i < len(results['id']):
        cat = results['omar category'][i]
        msg = unicode(results['message'][i].lower(), "utf-8")
        try:
            cat = int(cat)
        except:
            cat = None

        if cat == 1:
            pos_tweets.append((msg,'positive'))
        elif cat ==2 or cat == 3:
            other_tweets.append((msg,'other'))
        i+=1

    print "Number of positively labeled tweets: "+str(len(pos_tweets))
    print "Number of otherly labeled tweets: "+str(len(other_tweets))

    for (words, sentiment) in pos_tweets + other_tweets:
        words_filtered = clean_string(words)
        tweets.append((words_filtered, sentiment))

    #Uncomment the following few lines to calculate classifier accuracy
    # START ACCURACY CODE
    '''
    random.shuffle(tweets)
    num_tweets = len(tweets)
    percentage_test = 0.25
    cutoff = int(math.floor(num_tweets*percentage_test))
    print "Cutoff: "+str(cutoff)

    f1_set=tweets[cutoff:]
    total_set = nltk.classify.apply_features(extract_features,tweets)
    train_set = total_set[:cutoff]
    #test_set = total_set[cutoff:]
    classifier = nltk.NaiveBayesClassifier.train(train_set)

    for t in f1_set:
        #print t[0],t[1]
        real_cat = t[1]
        category = classifier.classify(extract_features(clean_string(' '.join(t[0]))))
        if 'positive' in category: int_cat = 1
        elif 'negative' in category: int_cat = 2
        else: int_cat=3

        if 'positive' in real_cat: real_cat = 1
        elif 'negative' in real_cat: real_cat = 2
        else: real_cat = 3

        #print real_cat, int_cat
        if real_cat == 1 and int_cat==1 : tp+=1
        if real_cat ==1 and (int_cat==2 or int_cat==3): fn+=1
        elif (real_cat==2 or real_cat==3) and (int_cat==2 or int_cat==3): tn+=1
        elif (real_cat==2 or real_cat==3) and (int_cat==1) : fp+=1
        #print category, s
    f1 = (2.0*tp) / (2*tp + fp + fn)
    recall = tp*1.0 / (tp+fn) 
    precision = tp*1.0 / (tp+fp)
    accuracy = (tp+tn)*1.0 / (tp+tn+fp+fn)

    print tp, tn, fp, fn
    print "F1 measure: ",f1
    print "Accuracy: ",accuracy
    print "Recall: ", recall, ", precision: ",precision
    return (f1, precision, recall, accuracy)
    '''
    # END ACCURACY CODE
    word_features = get_word_features(get_words_in_tweets(tweets))
    training_set = nltk.classify.apply_features(extract_features,tweets)
    classifier = nltk.NaiveBayesClassifier.train(training_set)

    test_tweets = parse_csv(ALL_CSV_FILE)  #parse_csv(CURRENT_CSV_FILE)
    num_positive = num_other = 0
    positive_num_prep = 0
    negative_num_prep = 0
    positive_followers = []
    positive_retweets = []
    negative_followers = []
    negative_retweets = []

    csvfile = open(CURRENT_CSV_FILE_A,"w")
    spamwriter = csv.writer(csvfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(fieldnames)

    i = 1
    while i < len(test_tweets['id']):
        category = classifier.classify(extract_features(clean_string(test_tweets['message'][i])))
        int_cat = 0
        if category == 'positive':
            num_positive +=1
            positive_followers.append(test_tweets['follower_count'][i])
            positive_retweets.append(test_tweets['retweet_count'][i])
            new_cat_labels.append(1)
            int_cat = 1
        elif category == 'other':
            num_other +=1
            negative_followers.append(test_tweets['follower_count'][i])
            negative_retweets.append(test_tweets['retweet_count'][i])
            new_cat_labels.append(3)
            int_cat = 3

        line = []
        for k in fieldnames:
            if k is 'category':
                line.append(str(int_cat))
            else:
                line.append(test_tweets[k][i])

        spamwriter.writerow(line)
        i+=1

    csvfile.close()

    '''
    f1 = (2.0*tp) / (2*tp + fp + fn)
    print "F1 measure: ", tp,tn,fp,fn
    print "F1 measure: ",f1
    print "Number of positive tweets: " + str(num_positive)
    print "Number of other tweets: "+ str(num_other)
    '''

def experiment_negative_or_other():
    neg_tweets = []
    other_tweets = []
    new_cat_labels = []
    tweets = []
    results = parse_csv(CURRENT_CSV_FILE)  #parse_csv([pap_csv_filename,colonoscopy_csv_filename,mammography_csv_filename]))
    #results = parse_csv([pap_csv_filename,colonoscopy_csv_filename,mammography_csv_filename])  #parse_csv(CURRENT_CSV_FILE)
    tp = 0
    tn = 0
    fp = 0
    fn = 0

    i = 1
    while i < len(results['id']):
        cat = results['omar category'][i]
        msg = unicode(results['message'][i].lower(), "utf-8")
        try:
            cat = int(cat)
        except:
            cat = None

        if cat == 2:
            neg_tweets.append((msg,'negative'))
        elif cat ==1 or cat == 3:
            other_tweets.append((msg,'other'))
        i+=1

    print "Number of negatively labeled tweets: "+str(len(neg_tweets))
    print "Number of otherly labeled tweets: "+str(len(other_tweets))

    for (words, sentiment) in neg_tweets + other_tweets:
        words_filtered = clean_string(words)
        tweets.append((words_filtered, sentiment))

    #Uncomment the following few lines to calculate classifier accuracy
    # START ACCURACY CODE
    '''
    random.shuffle(tweets)
    num_tweets = len(tweets)
    percentage_test = 0.25
    cutoff = int(math.floor(num_tweets*percentage_test))
    print "Cutoff: "+str(cutoff)

    f1_set=tweets[cutoff:]
    total_set = nltk.classify.apply_features(extract_features,tweets)
    train_set = total_set[:cutoff]
    #test_set = total_set[cutoff:]
    classifier = nltk.NaiveBayesClassifier.train(train_set)

    for t in f1_set:
        #print t[0],t[1]
        real_cat = t[1]
        category = classifier.classify(extract_features(clean_string(' '.join(t[0]))))
        if 'positive' in category: int_cat = 1
        elif 'negative' in category: int_cat = 2
        else: int_cat=3

        if 'positive' in real_cat: real_cat = 1
        elif 'negative' in real_cat: real_cat = 2
        else: real_cat = 3

        #print real_cat, int_cat
        if real_cat == 2 and int_cat==2 : tp+=1
        if real_cat ==2 and (int_cat==1 or int_cat==3): fn+=1
        elif (real_cat==1 or real_cat==3) and (int_cat==1 or int_cat==3): tn+=1
        elif (real_cat==1 or real_cat==3) and (int_cat==2) : fp+=1
        #print category, s
    f1 = (2.0*tp) / (2*tp + fp + fn)
    recall = tp*1.0 / (tp+fn) 
    precision = tp*1.0 / (tp+fp)
    accuracy = (tp+tn)*1.0 / (tp+tn+fp+fn)

    print tp, tn, fp, fn
    print "F1 measure: ",f1
    print "Accuracy: ",accuracy
    print "Recall: ", recall, ", precision: ",precision

    #return (f1, precision, recall, accuracy)

    '''
    # END ACCURACY CODE

    word_features = get_word_features(get_words_in_tweets(tweets))
    training_set = nltk.classify.apply_features(extract_features,tweets)
    classifier = nltk.NaiveBayesClassifier.train(training_set)

    test_tweets = parse_csv(ALL_CSV_FILE)  #parse_csv(CURRENT_CSV_FILE)

    num_negative = num_other = 0
    other_num_prep = 0
    negative_num_prep = 0
    other_followers = []
    other_retweets = []
    negative_followers = []
    negative_retweets = []

    csvfile = open(CURRENT_CSV_FILE_B,"w")
    spamwriter = csv.writer(csvfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(fieldnames)

    i = 1
    while i < len(test_tweets['id']):
        category = classifier.classify(extract_features(clean_string(test_tweets['message'][i])))
        int_cat = 0
        if category == 'negative':
            num_negative +=1
            negative_followers.append(test_tweets['follower_count'][i])
            negative_retweets.append(test_tweets['retweet_count'][i])
            new_cat_labels.append(2)
            int_cat = 2
        elif category == 'other':
            num_other +=1
            other_followers.append(test_tweets['follower_count'][i])
            other_retweets.append(test_tweets['retweet_count'][i])
            new_cat_labels.append(3)
            int_cat = 3
        #if cat : test_set.add(int_cat)

        line = []
        for k in fieldnames:
            if k is 'category':
                line.append(str(int_cat))
            else:
                line.append(test_tweets[k][i])

        spamwriter.writerow(line)
        i+=1

    csvfile.close()

    print "Number of negative tweets: " + str(num_negative)
    print "Number of other tweets: "+ str(num_other)

def analyze(filein):

    all_tweets = parse_csv(filein)
    pos_retweets = []
    neg_retweets = []
    neutral_retweets = []
    pos_followers = []
    neg_followers = []
    neutral_followers = []
    pos_favorite_count = []
    neg_favorite_count = []
    neutral_favorite_count = []
    pos_klout = []
    neg_klout = []
    neutral_klout = []
    kloutless = 0
    num_pos = num_neg = num_neutral = num_throwaway = 0
    fieldnames = ['id','tweet_id','screen_name','name','klout_score','message','created_at','follower_count','retweet_count','favorite_count','location','Sid category','omar category','category','query']
    pos_prep_related = 0
    pos_not_prep_related = 0
    neg_prep_related = 0
    neg_not_prep_related = 0
    neutral_prep_related = 0
    neutral_not_prep_related = 0
    prep_messages = []

    print "Analyzing file "+filein

    i = 1
    while i < len(all_tweets['id']):
        cat = int(all_tweets['category'][i])
        retweets = int(all_tweets['retweet_count'][i])
        followers = int(all_tweets['follower_count'][i])
        favorites = int(all_tweets['favorite_count'][i])
        message = all_tweets['message'][i]
        try:
            klout_score = float(all_tweets['klout_score'][i])
        except:
            klout_score = None
            kloutless +=1

        if contains_prep_word(message): prep_messages.append(message)

        if cat == 1:
            num_pos +=1
            pos_retweets.append(retweets)
            pos_favorite_count.append(favorites)
            pos_followers.append(followers)
            if klout_score: pos_klout.append(klout_score)
            if contains_prep_word(message): pos_prep_related +=1
            else: pos_not_prep_related +=1
        elif cat ==2:
            num_neg +=1
            neg_retweets.append(retweets)
            neg_favorite_count.append(favorites)
            neg_followers.append(followers)
            if klout_score: neg_klout.append(klout_score)
            if contains_prep_word(message): neg_prep_related +=1
            else: neg_not_prep_related +=1
        elif cat ==3:
            num_neutral +=1
            neutral_retweets.append(retweets)
            neutral_favorite_count.append(favorites)
            neutral_followers.append(followers)
            if klout_score: neutral_klout.append(klout_score)
            if contains_prep_word(message): neutral_prep_related +=1
            else: neutral_not_prep_related +=1
        elif cat ==4:
            num_throwaway +=1

        i+=1

    total = num_pos+num_neg+num_neutral+num_throwaway
    print "Summary:"
    print "Number of positive tweets:"+str(num_pos)+", "+str(float((1.0*num_pos/total)*100))+"%"
    print "Mean number of positive retweets: "+str(numpy.mean(pos_retweets))+", standard deviation: "+str(numpy.std(pos_retweets))
    print "Mean number of positive favorites: "+str(numpy.mean(pos_favorite_count))+", standard deviation: "+str(numpy.std(pos_favorite_count))
    print "Mean number of positive followers: "+str(numpy.mean(pos_followers))+", standard deviation: "+str(numpy.std(pos_followers))
    print "Mean number of positive klout scores: "+str(numpy.mean(pos_klout))+", standard deviation: "+str(numpy.std(pos_klout))
    print "------------------------------------------------------------------------------------------\n\n"
    print "Number of negative tweets:"+str(num_neg) +", "+str(float((1.0*num_neg/total)*100))+"%"
    print "Mean number of negative retweets: "+str(numpy.mean(neg_retweets))+", standard deviation: "+str(numpy.std(neg_retweets))
    print "Mean number of negative favorites: "+str(numpy.mean(neg_favorite_count))+", standard deviation: "+str(numpy.std(neg_favorite_count))
    print "Mean number of negative followers: "+str(numpy.mean(neg_followers))+", standard deviation: "+str(numpy.std(neg_followers))
    print "Mean number of negative klout scores: "+str(numpy.mean(neg_klout))+", standard deviation: "+str(numpy.std(neg_klout))
    print "------------------------------------------------------------------------------------------\n\n"
    print "Number of neutral tweets:"+str(num_neutral) +", "+str(float((1.0*num_neutral/total)*100))+"%"
    print "Mean number of neutral retweets: "+str(numpy.mean(neutral_retweets))+", standard deviation: "+str(numpy.std(neutral_retweets))
    print "Mean number of neutral favorites: "+str(numpy.mean(neutral_favorite_count))+", standard deviation: "+str(numpy.std(neutral_favorite_count))
    print "Mean number of neutral followers: "+str(numpy.mean(neutral_followers))+", standard deviation: "+str(numpy.std(neutral_followers))
    print "Mean number of neutral klout scores: "+str(numpy.mean(neutral_klout))+", standard deviation: "+str(numpy.std(neutral_klout))
    print "------------------------------------------------------------------------------------------\n\n"
    print "Number of throwaways: "+str(num_throwaway) +", "+str(float((1.0*num_throwaway/total)*100))+"%"
    print "Kloutless entries: "+str(kloutless)+", "+str(float(100.0*kloutless/total))+"%"
    print "Total tweets: "+str(total)
    if 'colo' in filein:
        print "Number of positive bowel prep related tweets: "+str(pos_prep_related)+", "+str(float((1.0*pos_prep_related/total)*100))+"%"
        print "Number of positive non-bowel prep related tweets: "+str(pos_not_prep_related) +", "+str(float((1.0*pos_not_prep_related/total)*100))+"%"
        print "Number of negative bowel prep related tweets: "+str(neg_prep_related)+", "+str(float((1.0*neg_prep_related/total)*100))+"%"
        print "Number of negative non bowel prep related tweets: "+str(neg_not_prep_related)+", "+str(float((1.0*neg_not_prep_related/total)*100))+"%"
        print "Number of neutral bowel prep related tweets: "+str(neutral_prep_related)+", "+str(float((1.0*neutral_prep_related/total)*100))+"%"
        print "Number of neutral non bowel prep related tweets: "+str(neutral_not_prep_related)+", "+str(float((1.0*neutral_not_prep_related/total)*100))+"%"
        #print prep_messages


def clean_string(string_in):
    #return tok.tokenize(string_in)
    return re.findall(r"[\w']+|[.,!?;]", string_in.rstrip().lower())

def contains_prep_word(word_in):
    found = False
    if 'drink' in word_in and 'stuff' in word_in: return True
    for w in prep_words:
        if w in word_in: return True
    return False

def generate_dp_lists(csv_file_in,dp_file_out):

    d = parse_csv(csv_file_in)

    pos_csvfile = open(dp_file_out,"w")
    spamwriter = csv.writer(pos_csvfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
    spamwriter.writerow(dp_fieldnames)
    neg_csvfile = open(dp_file_out+"_neg.csv","w")
    spamwriter2 = csv.writer(neg_csvfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
    spamwriter2.writerow(dp_fieldnames)
    neutral_csvfile = open(dp_file_out+"_neutral.csv","w")
    spamwriter3 = csv.writer(neutral_csvfile, delimiter=',',quotechar='"', quoting=csv.QUOTE_MINIMAL)
    spamwriter3.writerow(dp_fieldnames)

    i = 1
    while i < len(d1['id']):
        sn = d['screen_name'][i]
        cat = int(d1['category'][i])

        line = []

        for k in fieldnames:
            if k is 'category':
                line.append(str(final_cat))
            else:
                line.append(d1[k][i])

        spamwriter.writerow(line)
        i+=1

    csvfile.close()
    print "Number positive: "+str(num_pos)
    print "Number negative: "+str(num_neg)
    print "Number neutral: "+str(num_neutral)
    print "Number conflicting: "+str(num_conflicting)


def gen_sentiment_vs_modality_bargraphs():

    colo_pos = 755
    colo_neg = 1791
    colo_neutral = 7716
    pap_pos = 1490
    pap_neg = 1082
    pap_neutral = 8011
    mammo_pos = 1185
    mammo_neg = 179
    mammo_neutral = 10638
    colo = (colo_pos,colo_neg,colo_neutral)
    pap = (pap_pos,pap_neg,pap_neutral)
    mammo = (mammo_pos,mammo_neg,mammo_neutral)
    pos = (colo_pos,pap_pos,mammo_pos)
    neg = (colo_neg,pap_neg,mammo_neg)
    neutral = (colo_neutral,pap_neutral,mammo_neutral)

    # ordered by colo (pos,neg,neutral), pap, mammo
    pos_perc_text = ['7.4%','14.1%','9.9%']
    neg_perc_text = ['17.5%','10.2%','1.5%']
    neutral_perc_text = ['75.2%','75.7%','88.6%']

    colo_percentage_text = ['7.4%','17.5%','75.2%']
    pap_percentage_text = ['14.1%','10.2%','75.7%']
    mammo_percentage_text = ['9.9%','1.5%','88.6%']
    N = 3  #len(pos)

    ind = numpy.arange(N)
    width = 0.25

    fig, ax = plt.subplots()
    rects1 = ax.bar(ind, pos, width, color='g')
    rects2 = ax.bar(ind + width,neg,width,color='r')
    rects3 = ax.bar(ind+width*2,neutral,width,color='b')

    ax.set_ylabel('Number of tweets')
    ax.set_title('Sentiment associated with cancer screening modalities')
    ax.set_xticks(ind + width)
    ax.set_xticklabels(('Colonoscopy','Pap smear','Mammography'))

    ax.legend((rects1[0], rects2[0], rects3[0]), ('Positive sentiment','Negative sentiment','Neutral sentiment'),loc="upper left")

    autolabel(rects1,pos_perc_text,ax)
    autolabel(rects2,neg_perc_text,ax)
    autolabel(rects3,neutral_perc_text,ax)
    #fig.tight_layout()

    plt.savefig("Sentiment associated with cancer screening modalities")

def autolabel(rects,bar_labels,axis):
    # attach some text labels
    i=0
    for rect in rects:
        height = rect.get_height()
        axis.text(rect.get_x() + rect.get_width()/2., 1.05*height,bar_labels[i],ha='center', va='bottom')
        i+=1





'''
This source code was used to collect and analyze data for
"Using Social Media to Characterize Public Sentiment toward Cancer Screening Interventions" by Metwally O, Blumberg S, Ladabaum U, and Sinha SR. (JMIR)

Code that collects tweets is original.

Code that utilizes NLTK's Naive Bayes classifiers is based on the following:

http://www.laurentluce.com/posts/twitter-sentiment-analysis-using-python-and-nltk/

*Copyright and Licensing Information*

Attribution-NonCommercial 3.0 United States (CC BY-NC 3.0 US)
This is a human-readable summary of (and not a substitute for) the license. Disclaimer.


You are free to:

Share — copy and redistribute the material in any medium or format

Adapt — remix, transform, and build upon the material
The licensor cannot revoke these freedoms as long as you follow the license terms under the following terms:

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

NonCommercial — You may not use the material for commercial purposes.

No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.
Notices:

You do not have to comply with the license for elements of the material in the public domain or where your use is permitted by an applicable exception or limitation.

No warranties are given. The license may not give you all of the permissions necessary for your intended use. For example, other rights such as publicity, privacy, or moral rights may limit how you use the material.


Omar Metwally, M.D.
omar.metwally@gmail.com

'''

from twython import Twython
import time, datetime, numpy
import re, math, collections, itertools, os
import nltk, nltk.classify.util, nltk.metrics
from nltk.classify import NaiveBayesClassifier
from nltk.metrics import BigramAssocMeasures
from nltk.probability import FreqDist, ConditionalFreqDist
from klout import *
import urllib, random
from yahoo_finance import Share
from Bio.Entrez import efetch, esearch, email
from Bio.Entrez import read as Eread
import datetime, unicodedata

APP_KEY = 'YOUR_APP_KEY'  
APP_SECRET = 'YOUR_APP_SECRET' 
OAUTH_TOKEN = 'YOUR_OAUTH_TOKEN'
OAUTH_TOKEN_SECRET = 'YOUR_TOKEN_SECRET' 

word_features = []
document_words = None
all_tweets = []

def index():

    #form = SQLFORM.factory(Field('name'), Field('email'), Field('phone'), Field('question','text') )
    #response.flash = form.element('textarea') 
    
    #form.element('textarea[name=question]')['_style'] = 'width:150px;height:50px;'    

    return dict()

def populate_klout_scores():
    twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    tweets = db(db.mammography_tweets).select(limitby=(0,10)) 
    #k = Klout('')
    scores = []

    for tweet in tweets:
        screen_name = tweet['screen_name']
        
        kloutId = k.identity.klout(screenName=screen_name).get('id')
        score = k.user.score(kloutId=kloutId).get('score')
        scores.append(score)
        time.sleep(0.5)
    response.flash = "mean klout score for mammography_tweets: " + str(sum(scores))
    
    return dict()
    
def klout():
    twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    positive_retweet_count = []
    positive_klout_score = []

#    result = twitter.lookup_status(id=600287796440399873)
    #screen_name = result[0]['entities']['screen_name']
    '''
    tweets = db(db.tweets).select(orderby=~db.tweets.retweet_count,limitby=(0,10)) 
    
    result = twitter.lookup_status(id=tweets[1]['tweet_id'])
    screen_name = result[0]['user']['screen_name']
    '''
    #k = Klout('')
    
    # Get kloutId of the user by inputting a twitter screenName
    kloutId = k.identity.klout(screenName='cdc_cancer').get('id')

    # Get klout score of the user
    score = k.user.score(kloutId=kloutId).get('score')
    
    response.flash = "klout score: " + str(score)
    
    return dict()

#@auth.requires_login()
def populate_colonoscopy():
    
    twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    query = 'colonoscopy'
    query_list = ['colonoscopy', 'screening colonoscopy', 'first colonoscopy', 'surveillance colonoscopy', 'crohnâ€™s disease AND colonoscopy', 'colonoscopy AND CD', 'colonoscopy', 'crohnâ€™s AND colonoscopy', 'ulcerative colitis AND colonoscopy', 'colonoscopy AND UC', 'IBD AND colonoscopy', 'colonoscopy AND magnesium citrate', 'colonoscopy AND golytely', 'colonoscopy AND halflytely', 'colonoscopy AND miralax', 'colonoscopy AND moviprep', 'colonoscopy AND low volume prep']

    if not session.query_index_colonoscopy: session.query_index_colonoscopy = 0
    #session.next_counter moves on to the next query term in the list after X refreshes        
    if not session.next_counter_colonoscopy: session.next_counter_colonoscopy = 0 
    #session.query_index_colonoscopy = random.randint(0,len(query_list)-1)
    if session.query_index_colonoscopy > len(query_list)-1: session.query_index_colonoscopy = 0
    query = urllib.quote_plus(query_list[session.query_index_colonoscopy])   

    if not session.lowest_min_id_colonoscopy:
        result = twitter.search(q=query,count=100,lang='en')
        if len(result['statuses']) > 0:
            session.lowest_min_id_colonoscopy = result['statuses'][0]['id']
    else:
        result = twitter.search(q=query,count=100,lang='en',max_id=session.lowest_min_id_colonoscopy)
    
    for x in result['statuses']:
        if x['id'] < session.lowest_min_id_colonoscopy: session.lowest_min_id_colonoscopy = x['id']            

        dt = datetime.datetime.strptime(x['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
        #db.colonoscopy_tweets.validate_and_insert(tweet_id=x['id'], message=x['text'], created_at=dt, followers_count=x['user']['followers_count'], retweet_count=x['retweet_count'], screen_name = x['user']['screen_name'] )
        
        db.colonoscopy_v2.validate_and_insert(tweet_id=x['id'], message=x['text'], created_at=dt, followers_count=x['user']['followers_count'], retweet_count=x['retweet_count'], favorite_count=x['favorite_count'], screen_name = x['user']['screen_name'], name=x['user']['name'], location=x['user']['location'], query=query )

    session.next_counter_colonoscopy +=1
    if session.next_counter_colonoscopy > 10:
        session.next_counter_colonoscopy = 0
        session.query_index_colonoscopy +=1
        session.lowest_min_id_colonoscopy = None
    
    response.flash = "query term: "+query+" with "+str(twitter.get_lastfunction_header('x-rate-limit-remaining'))+" remaining queries in this 15 minute window"+", next_counter_colonoscopy: "+str(session.next_counter_colonoscopy)+", query_index: "+str(session.query_index_colonoscopy)+", lowest_min_id_colonoscopy:"+str(session.lowest_min_id_colonoscopy)                      
            
    return dict()

# populate mammography tweets
def populate_ibs():
    
    twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    query = ''
    query_list = ['doctor','surgery','alone','married','marriage','single','elderly','children','child','sick day','call in sick','called in sick','vaccine','vaccination','colonoscopy','pap smear','pap test','mammogram','mammography','ct scan','x-ray','xray','MRI','irritable bowel syndrome','ibs','depressed','depression','anxiety','stress','ocd','obsessive compulsive disorder','schizophrenia','heart attack','stroke','migraine','epilepsy','seizure','pregnant','pregnancy','GERD','heartburn','diarrhea','constipation','constipated','nausea','bloated','glaucoma','cataract','influenza','the flu','strep throat','sore throat','fever','cancer','leukemia','lymphoma','addiction','copd','diabetes','myocardiac infarction','arthritis','kidney disease','ESRD','hypertension','heart disease','dementia','influenza','pneumonia','suicide','alzheimer','inflammatory bowel disease','IBD','crohn','ulcerative colitis','celiac disease','lupus', 'rheumatoid arthritis', 'psoriasis','autoimmune disease','chf','heart failure','ulcer','std','healthy','exercise','medication','meds','thyroid','mononucleosis','infection','death','die','happy','angry','pissed','livid','outraged','sad','afraid','worried','disgust','frustrated','impatient','hate','blood','urine','saliva','poop','feces','prostate','kidney','alcohol','marijuana','weed','lsd','mushrooms','acid','drugs','pharmacy','hospital','clinic','lab test','lft','cbc','blood test','drug test','breathing','wheezing','edema','bile','sperm','semen','testicle','sex','spirit','religion','pray','brain','head','mouth','throat','stomach','liver','NASH','NAFLD','fatty liver','gallbladder','bowel','anus','bladder','penis','vagina','lung','heart','tongue','bone','nerve','artery','vein','hurt','pain','esophagus','joints','skin','relief','work','job','family','home','food','eat','meal','obese','lose weight','overweight','meal','party','body','mind','weight','menstruation','menstrual','abdomen','gut']

    if not session.query_index_ibs: session.query_index_ibs = 0
    if not session.next_counter_ibs: session.next_counter_ibs = 0
    #session.query_index = random.randint(0,len(query_list)-1)        
    if session.query_index_ibs > len(query_list)-1: session.query_index_ibs = 0
    query = urllib.quote_plus(query_list[session.query_index_ibs])   

    if not session.lowest_min_id_ibs:
        result = twitter.search(q=query,count=100,lang='en')
        if len(result['statuses'])>0: session.lowest_min_id_ibs = result['statuses'][0]['id']
    else:
        result = twitter.search(q=query,count=100,max_id=session.lowest_min_id_ibs,lang='en')
    
    for x in result['statuses']:
        if x['id'] < session.lowest_min_id_ibs: session.lowest_min_id_ibs = x['id']
        #kloutId = k.identity.klout(screenName=x['user']['screen_name']).get('id')
        #kloutScore = k.user.score(kloutId=kloutId).get('score') 
        safe_text = x['text'].encode("utf-8").decode("utf-8")

        dt = datetime.datetime.strptime(x['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
        try:
            db.ibs_tweets2.validate_and_insert(tweet_id=x['id'], message=safe_text, created_at=dt, followers_count=x['user']['followers_count'], retweet_count=x['retweet_count'], favorite_count=x['favorite_count'], screen_name = x['user']['screen_name'], name=x['user']['name'], location=x['user']['location'], query=query )
        except:
            response.flash = "Error!"

    session.next_counter_ibs +=1
    if session.next_counter_ibs > 10:
        session.next_counter_ibs = 0
        session.query_index_ibs +=1
        session.lowest_min_id_ibs = None
        
    response.flash = "query term: "+query+" with "+str(twitter.get_lastfunction_header('x-rate-limit-remaining'))+" remaining queries in this 15 minute window"+", next_counter: "+str(session.next_counter_ibs)+", query_index_ibs: "+str(session.query_index_ibs)+", lowest_min_id_ibs: "+str(session.lowest_min_id_ibs)                      
            
    return dict()


# populate mammography tweets
def populate_mammography():
    
    twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    query = 'mammogram'
    query_list = ['mammogram', 'mammography']

    if not session.query_index_mammography: session.query_index_mammography = 0
    if not session.next_counter_mammography: session.next_counter_mammography = 0
    #session.query_index = random.randint(0,len(query_list)-1)        
    if session.query_index_mammography > len(query_list)-1: session.query_index_mammography = 0
    query = urllib.quote_plus(query_list[session.query_index_mammography])   

    if not session.lowest_min_id_mammography:
        result = twitter.search(q=query,count=100,lang='en')
        if len(result['statuses'])>0: session.lowest_min_id_mammography = result['statuses'][0]['id']
    else:
        result = twitter.search(q=query,count=100,max_id=session.lowest_min_id_mammography,lang='en')
    
    for x in result['statuses']:
        if x['id'] < session.lowest_min_id_mammography: session.lowest_min_id_mammography = x['id']
        #kloutId = k.identity.klout(screenName=x['user']['screen_name']).get('id')
        #kloutScore = k.user.score(kloutId=kloutId).get('score')                   

        dt = datetime.datetime.strptime(x['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
        db.mammography_v2.validate_and_insert(tweet_id=x['id'], message=x['text'], created_at=dt, followers_count=x['user']['followers_count'], retweet_count=x['retweet_count'], favorite_count=x['favorite_count'], screen_name = x['user']['screen_name'], name=x['user']['name'], location=x['user']['location'], query=query )

    session.next_counter_mammography +=1
    if session.next_counter_mammography > 10:
        session.next_counter_mammography = 0
        session.query_index_mammography +=1
        session.lowest_min_id_mammography = None
        
    response.flash = "query term: "+query+" with "+str(twitter.get_lastfunction_header('x-rate-limit-remaining'))+" remaining queries in this 15 minute window"+", next_counter: "+str(session.next_counter_mammography)+", query_index_mammography: "+str(session.query_index_mammography)+", lowest_min_id_mammography: "+str(session.lowest_min_id_mammograph)                      
            
    return dict()

# populate PAP tweets
def populate_pap():
    
    twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    query = 'pap smear'
    query_list = ['pap smear', 'pap test', 'pap screen', 'pap screening', 'papanicolau screen', 'papanicolau screening', 'papanicolau test', 'pap AND cervical cancer', 'pap AND pelvic exam', 'pap AND hpv', 'pap AND human papilloma virus']

    if not session.query_index_pap: session.query_index_pap = 0
    if not session.next_counter_pap: session.next_counter_pap = 0
        
    #session.query_index = random.randint(0,len(query_list)-1)        
    if session.query_index_pap > len(query_list)-1: session.query_index_pap = 0
    query = urllib.quote_plus(query_list[session.query_index_pap])       

    if not session.lowest_min_id_pap:
        result = twitter.search(q=query,count=100,lang='en')
        if len(result['statuses']) >0: session.lowest_min_id_pap = result['statuses'][0]['id']
    else:
        result = twitter.search(q=query,count=100,max_id=session.lowest_min_id_pap,lang='en')
    
    for x in result['statuses']:
        if x['id'] < session.lowest_min_id_pap: session.lowest_min_id_pap = x['id']
        #kloutId = k.identity.klout(screenName=x['user']['screen_name']).get('id')
        #kloutScore = k.user.score(kloutId=kloutId).get('score')                   

        dt = datetime.datetime.strptime(x['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
        db.pap_v2.validate_and_insert(tweet_id=x['id'], message=x['text'], created_at=dt, followers_count=x['user']['followers_count'], retweet_count=x['retweet_count'], favorite_count=x['favorite_count'], screen_name = x['user']['screen_name'], name=x['user']['name'], location=x['user']['location'], query=query )

    session.next_counter_pap +=1
    if session.next_counter_pap > 10:
        session.next_counter_pap = 0
        session.query_index_pap +=1
        session.lowest_min_id_pap = None
        
    response.flash = "query term: "+query+" with "+str(twitter.get_lastfunction_header('x-rate-limit-remaining'))+" remaining queries in this 15 minute window"+", next_counter_pap: "+str(session.next_counter_pap)+", query_index_pap: "+str(session.query_index_pap)+", "+str(session.lowest_min_id_pap)                      
            
    return dict()

def populate_stocks():
    
    twitter = Twython(APP_KEY, APP_SECRET, OAUTH_TOKEN, OAUTH_TOKEN_SECRET)
    query_list = ["Tesla :)","tsla :)","$TSLA :)","Tesla :(","tsla :(","$TSLA :("]
    #query_list = ["Tesla :(","tsla :(","$TSLA :("]
    

    stock_price = Share('TSLA').get_price()

    if not session.query_index_stock: session.query_index_stock = 0
    if not session.next_counter_stock: session.next_counter_stock = 0
        
    #session.query_index = random.randint(0,len(query_list)-1)        
    if session.query_index_stock > len(query_list)-1: session.query_index_stock = 0
    query = urllib.quote_plus(query_list[session.query_index_stock])       

    if not session.lowest_min_id_stock:
        result = twitter.search(q=query,count=100,lang='en')
        session.lowest_min_id_stock = result['statuses'][0]['id']
    else:
        result = twitter.search(q=query,count=100,max_id=session.lowest_min_id_stock,lang='en')
    
    for x in result['statuses']:
        if x['id'] < session.lowest_min_id_stock: session.lowest_min_id_stock = x['id']
        #kloutId = k.identity.klout(screenName=x['user']['screen_name']).get('id')
        #kloutScore = k.user.score(kloutId=kloutId).get('score')                   

        dt = datetime.datetime.strptime(x['created_at'], '%a %b %d %H:%M:%S +0000 %Y')
        if ":)" in query_list[session.query_index_stock]: cat = 1
        elif ":(" in query_list[session.query_index_stock]: cat = 2
        else: cat = 3
            
        db.stocks.validate_and_insert(tweet_id=x['id'], message=x['text'], created_at=dt, followers_count=x['user']['followers_count'], retweet_count=x['retweet_count'], favorite_count=x['favorite_count'], screen_name = x['user']['screen_name'], name=x['user']['name'], location=x['user']['location'], query=query,price=stock_price,category=cat )
            
    session.next_counter_stock +=1
    if session.next_counter_stock > 10:
        session.next_counter_stock = 0
        session.query_index_stock +=1
        session.lowest_min_id_stock = None
        
    response.flash = "query term: "+query+" with "+str(twitter.get_lastfunction_header('x-rate-limit-remaining'))+" remaining queries in this 15 minute window"+", next_counter_stock: "+str(session.next_counter_stock)+", query_index_stock: "+str(session.query_index_stock)+", lowest_min_id_stock: "+str(session.lowest_min_id_stock)+", stock price: "+str(stock_price+", sentiment category: "+str(cat)) #session.lowest_min_id                      
            
    return dict()
'''
def populate_pubmed():
    query_list = ['ibs']
    query = query_list[0]
    handle = esearch(db='pubmed', sort='relevance', retmax=20, retmode='xml', term=query)    
    email = 'xyz@xyz.com'
    handle = efetch(db='pubmed', id=form.vars.pmid, retmode='xml', rettype='abstract')
    xml_data = read(handle)[0]
    article = xml_data['MedlineCitation']['Article']
    if 'Abstract' in article.keys(): abstract = article['Abstract']['AbstractText']
    else: abstract = []
'''
def get_words_in_tweets(tweets):
    all_words = []
    for (words, sentiment) in tweets:
        all_words.extend(words)
    return all_words

def get_word_features(wordlist):
    wordlist = nltk.FreqDist(wordlist)
    word_features = wordlist.keys()
    return word_features

def extract_features(document):
    global document_words

#    if 'document_words' not in globals():
    pos_tweets = []
    neg_tweets = []
#    tweets = []
    
    if len(all_tweets) == 0:
        results = db(db.tweets.category==1).select()
        for result in results: pos_tweets.append((result.message,'positive'))
        results = db(db.tweets.category==2).select()
        for result in results: neg_tweets.append((result.message,'negative'))

        for (words, sentiment) in pos_tweets + neg_tweets:
            words_filtered = clean_string(words) #[e.lower() for e in words.split() if len(e) >= 3]
            all_tweets.append((words_filtered, sentiment))
    
    document_words = set(document)
    
    features = {}
 
    word_features = get_word_features(get_words_in_tweets(all_tweets))
    for word in word_features:
        features['contains(%s)' % word] = (word in document_words)
    return features
